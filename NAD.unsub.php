<?php

/**
 * Project:   NAD - The Notifications After Download System
 * File:      NAD.unsub.php 
 * Date:      29.03.2012
 * 
 * @package   NAD
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * NAD - script of unsubscription.
 * Used for request via webpage
 * (e.g. http://atompark.com/unsubscribe/).
 *
 * @package   NAD 
 * @author    Victor Burak <vb@atompark.com>
 */

require_once(dirname(__FILE__) . '/NAD.bootstrap.php');

/* @var $nad NADSystem */
$nad->unsubscribe();
$nad->beforeExit();

//pr(spl_autoload_functions());
//d(BDatabaseS::getDebugInfo());
