<?php

/**
 * Project:   NAD - The Notifications After Download System
 * File:      NADDownloadsModel.php 
 * Date:      14.03.2012
 * 
 * @package   NAD
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * NAD downloads model.
 *
 * @uses      BModelAbstract
 * @package   NAD 
 * @author    Victor Burak <vb@atompark.com>
 */
class NADDownloadModel extends BModelAbstract {

    /**
     * Subscriber status 'active (subscribed)'.
     * 
     * @var integer
     */
    const NAD_SUBSCRIBE_STATUS_ACTIVE   = 1;

    /** 
     * Subscriber status 'inactive (unsubscribed)'.
     * 
     * @var integer
     */
    const NAD_SUBSCRIBE_STATUS_INACTIVE = 0;

    /** 
     * Subscriber status 'unsubscribed from all'.
     * 
     * @var integer
     */
    const NAD_SUBSCRIBE_STATUS_NOTHING  = -1;
    
    /**
     * Class constructor. 
     * 
     * Initializes the linked table and the data model.
     * 
     * @return void
     */
    public function __construct() {
        $this->setDbTable(new NADDownloadsTable())->initModelData();
    }

    /**
     * Get subscriber by email and status.
     * Used for checking active subscriptions for this email address.
     * 
     * @param  string  $email
     * @param  integer $status default is NAD_SUBSCRIBE_STATUS_ACTIVE
     * @return boolean Returns TRUE for successful query or FALSE otherwise.
     */
    public function getOneByEmail($email, $status = self::NAD_SUBSCRIBE_STATUS_ACTIVE) {
        $where  = "`Email` = '{$email}' AND `Subscribe` = {$status}";
        $result = $this->getDbTable()->select($where);
        
        if (!empty($result)) {
            $this->setModelData($result[0]);
            return TRUE;
        } else {
            $this->initModelData();
            return FALSE;
        }
    }
    
    /**
     * Get subscriber by email, code and status.
     * 
     * @param  string  $email
     * @param  string  $code
     * @param  integer $status default is NAD_SUBSCRIBE_STATUS_ACTIVE
     * @return boolean Returns TRUE for successful query or FALSE otherwise.
     */
    public function getOneByEmailAndCode($email, $code, $status = self::NAD_SUBSCRIBE_STATUS_ACTIVE) {
        $where  = "`Email` = '{$email}' AND `Code` = '{$code}' AND `Subscribe` = {$status}";
        $result = $this->getDbTable()->select($where);
        
        if (!empty($result)) {
            $this->setModelData($result[0]);
            return TRUE;
        } else {
            $this->initModelData();
            return FALSE;
        }
    }
    
    /**
     * Get all subscribers for specified template.
     * 
     * Must have a downloaded products within a strict date range.
     * The date range (count of days after download) is equal to the $template->type.
     * 
     * @param  NADTemplateModel $template
     * @return NADDownloadCollection 
     */
    public function getAllSubscribers(NADTemplateModel $template) {
        $abbrAnalog = AbbrHelper::getAnalog($template->abbr);
        $where      = "(`Program` = '{$template->abbr}' OR `Program` = '{$abbrAnalog}') "
                    . "AND `Subscribe` = 1 AND (TO_DAYS(NOW()) - TO_DAYS(`DTime`)) = {$template->type}";
                    //. " AND `Country` = 'RU'"; // @TODO: remove after debug
        $result     = $this->getDbTable()->select($where);
        $collection = new NADDownloadCollection();
        
        if (!empty($result)) {
            foreach ($result as $row) {
                $download = new $this;
                $download->setModelData($row);
                $collection->add($download);
            }
        }
                
        return $collection;
    }

    /**
     * Checks for the matching the template's language 
     * and a downloader's country code.
     * 
     * Now 'RU' language is allowed for all CIS countries, for others is used 'EN'.
     * If need to add new language - just have to add new case in the 'switch' operator.
     * 
     * @param  NADTemplateModel $template
     * @return boolean Returns TRUE for clearly matching or FALSE otherwise.
     */
    public function langMatchesTemplate(NADTemplateModel $template) {
        $retval = FALSE;
        
        switch ($template->lang) {
            case 'RU':
                if ($this->isCISCountry($this->Country)) {
                    $retval = TRUE;
                }
                break;
            case 'EN':
                if (!$this->isCISCountry($this->Country)) {
                    $retval = TRUE;
                }
                break;
            case 'ES':
                if ($this->isSpicCountry($this->Country)) {
                    $retval = TRUE;
                }
                break;
            default:
                break;
        }
        
        return $retval;
    }

    /**
     * Checks if country code is one of CIS (SNG) countries 
     * and can be replaced with a russian language for template.
     * 
     * @param  string $code
     * @return boolean
     */
    public function isCISCountry($code) {
        $countriesCIS = array(
            'AZ' => 'Азербайджан',
            'AM' => 'Армения',
            'BY' => 'Белоруссия',
            'KZ' => 'Казахстан',
            'KG' => 'Киргизия',
            'MD' => 'Молдавия',
            'RU' => 'Россия',
            'TJ' => 'Таджикистан',
            'TM' => 'Туркмения',
            'UZ' => 'Узбекистан',
            'UA' => 'Украина',
            'GE' => 'Грузия',
        );
        return array_key_exists($code, $countriesCIS);
    }
    
    /**
     * Checks if country code is one of spic countries (spanish-language) 
     * and can be replaced with a spanish language for template.
     * 
     * Country list formed according to info about Hispanidad
     * from http://en.wikipedia.org/wiki/Hispanidad.
     * 
     * @param  string $code
     * @return boolean 
     */
    public function isSpicCountry($code) {
        $countriesSpic = array(
            'AR' => 'Аргентина',
            'BO' => 'Боливия',
            'CL' => 'Чили',
            'CO' => 'Колумбия',
            'CR' => 'Коста-Рика',
            'CU' => 'Куба',
            'DO' => 'Доминиканская Республика',
            'EC' => 'Эквадор',
            'SV' => 'Сальвадор',
            'GT' => 'Гватемала',
            'HN' => 'Гондурас',
            'MX' => 'Мексика',
            'NI' => 'Никарагуа',
            'PA' => 'Панама',
            'PY' => 'Парагвай',
            'PE' => 'Перу',
            'PR' => 'Пуэрто-Рико',
            'UY' => 'Уругвай',
            'VE' => 'Венесуэла',
            'ES' => 'Испания',
            'PH' => 'Филиппины',
            'GQ' => 'Экваториальная Гвинея',
            'EH' => 'Западная Сахара',
        );
        
        return array_key_exists($code, $countriesSpic);
    }
    
    /**
     * Send the first email for subscriber and update sent log.
     * 
     * @param  NADTemplateModel $template 
     * @return void
     */
    public function sendMail(NADTemplateModel $template) {
        $readyText = $template->getReadyText($this);

        /* @var $mail BMail */
        $mail = NADSystem::getRegistry()->get('MAIL');
        $mail->clear()
             ->setFrom($template->from_email)
             ->addTo($this->Email)
             ->setSubject($template->subject)
             ->setReturnPath($template->from_email)
             ->setBodyText($readyText)
             ->setMyHost('epochta.ru')
             ->send();
        
        $mail->clear()
             ->setFrom($template->from_email)
             ->addTo('vb@atompark.com') // @TODO remove after debugging
             ->setSubject('[Копия] ' . $template->subject)
             ->setReturnPath($template->from_email)
             ->setBodyText($readyText)
             ->setMyHost('epochta.ru')
             ->send();
        
        $this->updateSentLog($template);
    }

    /**
     * Send confirmation email with direct link to unsubscribe.
     * 
     * @return void
     */
    public function sendConfirmationMail() {
        $lang    = $this->detectLang();
        $url     = $lang == 'RU' ? 'http://epochta.ru/unsubscribe/' : 'http://atompark.com/unsubscribe/';
        $from    = $lang == 'RU' ? 'tech@epochta.ru' : 'tech@atompark.com';
        $subject = $lang == 'RU' ? 'Подтвердите отписку от рассылок' : 'Confirm the unsubscription';
        $myHost  = $lang == 'RU' ? 'epochta.ru' : 'atompark.com';

        $tpl = new BTemplate();
        $tpl->setDir(NADSystem::getRegistry()->get('TPLPATH'));
        $tplFileName = 'unsub_confirm_letter_' . strtolower($lang) . '.phtml';
        $tpl->setFile($tplFileName);
        $tpl->set('url',   $url);
        $tpl->set('email', $this->Email);
        $tpl->set('code',  $this->Code);
        $mailBody = $tpl->parse();
        
        /* @var $mail BMail */
        $mail = NADSystem::getRegistry()->get('MAIL');
        $mail->setFrom($from)
             ->addTo($this->Email)
             ->setSubject($subject)
             ->setReturnPath($from)
             ->setBodyText($mailBody)
             ->setMyHost($myHost)
             ->send();
    }
    
    /**
     * Checks whether the letter has been already sent to this downloader.
     * 
     * @param  NADTemplateModel $template 
     * @return boolean Returns TRUE if this template already sent or FALSE otherwise.
     */
    public function alreadySent(NADTemplateModel $template) {
        $sentLog = $this->getSentLog();
        $retval  = FALSE;
        
        if (!empty($sentLog)) {
            $nowDate = date('d.m.Y');
            foreach ($sentLog as $row) {
                $sentDate = substr($row['sent_date'], 0, 10);
                if ($row['tpl_id'] == $template->id 
                    AND $row['tpl_type'] == $template->type
                    AND $sentDate == $nowDate) {
                    $retval = TRUE;
                }
            }
        }
        
        return $retval;
    }
    
    /**
     * Update log of sent emails.
     * 
     * Statistics of sent messages stored in the `nad_sent` field 
     * as serialized arrays ['tpl_id' = 10, 'msg_type' => 1, 'sent_date' = '01.01.2012 12:00:00'].
     * 
     * @param  NADTemplateModel $template 
     * @return void
     */
    public function updateSentLog(NADTemplateModel $template) {
        $sentLog = $this->getSentLog();
        $sentLog[] = array(
            'tpl_id'    => $template->id,
            'tpl_type'  => $template->type,
            'sent_date' => date('d.m.Y H:i:s'),
        );
        $this->nad_sent = serialize($sentLog);
        $this->save();
    }

    /**
     * Returns a log of sent letters as array.
     * 
     * Statistics of sent messages stored in the `nad_sent` field 
     * as serialized arrays ['tpl_id' = 10, 'msg_type' => 1, 'sent_date' = '01.01.2012 12:00:00'].
     * 
     * @return array 
     */
    public function getSentLog() {
        if ($this->nad_sent) {
            $sentLog = unserialize($this->nad_sent);
        } else {
            $sentLog = array();
        }
        return $sentLog;
    }

    /**
     * Detect language for this downloader basis on his country code.
     * 
     * @return string 'EN', 'RU' or 'ES'
     */
    public function detectLang() {
        if ($this->isCISCountry($this->Country)) {
            $lang = 'RU';
        } elseif ($this->isSpicCountry($this->Country)) {
            $lang = 'ES';
        } else {
            $lang = 'EN';
        }
        
        return $lang;
    }
    
    /**
     * Unsubscribe the downloader from one product.
     * 
     * Important! Model data must be loaded before.
     * 
     * @return boolean Returns TRUE for successful operation or FALSE otherwise.
     */
    public function unsubscribeOne() {
        /* @var $logger BFileLogger */
        $logger = NADSystem::getRegistry()->get('LOG');
        $logger->write("- Адрес: {$this->Email} отписался от рассылки по программе: {$this->Program}.");
        $this->Subscribe = self::NAD_SUBSCRIBE_STATUS_INACTIVE;
        return $this->save();
    }

    /**
     * Unsubscribe the downloader from all products.
     * 
     * Important! Model data must be loaded before.
     * 
     * @return boolean Returns TRUE for successful operation or FALSE otherwise.
     */
    public function unsubscribeAll() {
        /* @var $logger BFileLogger */
        $logger = NADSystem::getRegistry()->get('LOG');
        $logger->write("- Адрес: {$this->Email} отписался от всех рассылок.");
        
        /* @var $dbAdapter BDatabaseInterface */
        $dbAdapter = NADSystem::getRegistry()->get('DB1');
        $tableName = $this->getDbTable()->getTableName();
        $status    = self::NAD_SUBSCRIBE_STATUS_NOTHING;
        $query     = "UPDATE `{$tableName}` SET `Subscribe` = {$status} WHERE `Email` = '{$this->Email}'";
        
        return $dbAdapter->query($query);
    }

    /**
     * Check for orders of this user for last 10 days.
     * 
     * @return boolean
     */
    public function hasOrderedProductsForLast10Days() {
        $retval = FALSE;
        
        /* @var $dbAdapter BDatabaseInterface */
        $dbAdapter = NADSystem::getRegistry()->get('DB1');
        $query     = "SELECT * FROM `orders` "
                   . "WHERE `OrderEmail`='{$this->Email}' "
                   . "AND (TO_DAYS(NOW()) - TO_DAYS(`OrderDate`)) <= 10";
        
        $dbAdapter->query($query);
        $result =  $dbAdapter->getRowsetAsArray();
        
        empty($result) OR $retval = TRUE;
        
        return $retval;
    }

}

