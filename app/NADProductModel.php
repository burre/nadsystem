<?php

/**
 * Project:   NAD - The Notifications After Download System
 * File:      NADProductModel.php 
 * Date:      21.03.2012
 * 
 * @package   NAD
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * NADProductsModel
 *
 * @uses      BModelAbstract
 * @package   NAD 
 * @author    Victor Burak <vb@atompark.com>
 */
class NADProductModel extends BModelAbstract {

    /**
     * Class constructor.
     * 
     * @return void 
     */
    public function __construct() {
        $this->setDbTable(new NADProductsTable())->initModelData();
    }

    /**
     * Returns abbreviation for specified program by Id.
     * 
     * @param integer $progId
     * @return string 
     */
    public function getAbbrByProgId($progId) {
        $result = $this->getDbTable()->findByPrimaryKey($progId);
        if (!empty($result)) {
            $this->setModelData($result);
        } else {
            $this->initModelData();
        }
        return $this->abbr;
    }

}
