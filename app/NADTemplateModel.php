<?php

/**
 * Project:   NAD - The Notifications After Download System
 * File:      NADTemplateModel.php 
 * Date:      14.03.2012
 * 
 * @package   NAD
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * NAD message template model.
 *
 * @property string $lang
 * @uses      BModelAbstract
 * @package   NAD 
 * @author    Victor Burak <vb@atompark.com>
 * 
 * @property string $parsedText Fully parsed text (extra-field, see __constructor).
 */
class NADTemplateModel extends BModelAbstract {
    /**
     * Template status 'active'.
     * 
     * @var integer 
     */

    const NAD_TPL_ACTIVE = 1;

    /**
     * Template status 'inactive'.
     * 
     * @var integer 
     */
    const NAD_TPL_INACTIVE = 0;

    private $parsedText = '';
            
    /**
     * Class constructor.
     * 
     * Initializes the linked table and the data model.
     * 
     * @return void 
     */
    public function __construct() {
        $this->setDbTable(new NADTemplatesTable());
//        $this->setExtraField('parsedText', '');
        $this->initModelData();
    }

    /**
     * Get collection of NADTemplateModel with active status.
     * 
     * @return NADTemplateCollection
     */
    public function getAllActive() {
        $where = '`status` = ' . self::NAD_TPL_ACTIVE;
        $order = '`id`';
        $result = $this->getDbTable()->select($where, $order);
        $collection = new NADTemplateCollection();

        if (!empty($result)) {
            foreach ($result as $row) {
                $template = new $this;
                $template->setModelData($row);
                $collection->add($template);
            }
        }

        return $collection;
    }

    /**
     * Get collection of NADTemplateModel with certain type.
     * 
     * @param  integer $type
     * @return NADTemplateCollection
     */
    public function getAllByType($type = 1) {
        $where = "`type` = $type AND `status` = " . self::NAD_TPL_ACTIVE;
        $order = '`id`';
        $result = $this->getDbTable()->select($where, $order);
        $collection = new NADTemplateCollection();

        if (!empty($result)) {
            foreach ($result as $row) {
                $template = new $this;
                $template->setModelData($row);
                $collection->add($template);
            }
        }

        return $collection;
    }

    /**
     * Get one template by the program abbreviation (A), template type (T) and language (L).
     * 
     * @param  string  $abbr
     * @param  integer $type 
     * @param  string  $lang 
     * @return boolean Returns TRUE for successful query or FALSE otherwise.
     */
    public function getOneByATL($abbr, $type, $lang) {
        if ($abbr AND $type AND $lang) {
            $where = "`abbr` = '{$abbr}' AND `type` = {$type} AND `lang` = '{$lang}' AND `status` = " . self::NAD_TPL_ACTIVE;
            $result = $this->getDbTable()->select($where);

            if (!empty($result)) {
                $this->setModelData($result[0]);
                return TRUE;
            } else {
                $this->initModelData();
                return FALSE;
            }
        }
    }

    /**
     * Replaces some meta variables by values from NADDownloadModel.
     * 
     * @param NADDownloadModel $download 
     * @return void
     */
    private function _replaceMetaVariables(NADDownloadModel $download) {
        $subscriberName = trim($download->firstname . ' ' . $download->lastname);
        
        if (empty($subscriberName)) {
            switch ($this->lang) {
                case 'ES':
                    $subscriberName = 'suscriptor';
                    break;
                case 'RU':
                    $subscriberName = 'подписчик';
                    break;
                default:
                    $subscriberName = 'subscriber'; // default is 'EN'
                    break;
            }
        }
        $subscriberName = mb_convert_encoding($subscriberName, 'CP1251', 'UTF-8');

        $text = stripslashes($this->template);
        $text = str_replace('%SITE%',         $download->Site, $text);
        $text = str_replace('%NAME%',         $subscriberName, $text);
        $text = str_replace('%RU_PHONE%',     NADSystem::getRegistry()->get('nad.ru_phone'), $text);
        $text = str_replace('%RU_FREEPHONE%', NADSystem::getRegistry()->get('nad.ru_freephone'), $text);
        $text = str_replace('%EN_PHONE%',     NADSystem::getRegistry()->get('nad.en_phone'), $text);
        $text = str_replace('%RU_TIME_FROM%', NADSystem::getRegistry()->get('nad.ru_time_from'), $text);
        $text = str_replace('%RU_TIME_TO%',   NADSystem::getRegistry()->get('nad.ru_time_to'), $text);
        $text = str_replace('%EN_TIME_FROM%', NADSystem::getRegistry()->get('nad.en_time_from'), $text);
        $text = str_replace('%EN_TIME_TO%',   NADSystem::getRegistry()->get('nad.en_time_to'), $text);
        $text = str_replace('%RU_WEEKDAYS%',  mb_convert_encoding(NADSystem::getRegistry()->get('nad.ru_weekdays'), 'CP1251', 'UTF-8'), $text);
        $text = str_replace('%EN_WEEKDAYS%',  NADSystem::getRegistry()->get('nad.en_weekdays'), $text);
        $this->parsedText = $text;
    }

    /**
     * Returns a template block with unsubscribe instructions.
     * 
     * @param NADDownloadModel $download 
     * @return void
     */
    private function _addUnsubscribeBlock(NADDownloadModel $download) {
        $tplFileName = 'unsub_block_' . strtolower($this->lang) . '.phtml';
        $url = $download->detectLang() == 'RU' ? 'http://epochta.ru/unsubscribe/' : 'http://atompark.com/unsubscribe/';

        $tpl = new BTemplate();
        $tpl->setDir(NADSystem::getRegistry()->get('TPLPATH'));
        $tpl->setFile($tplFileName);
        $tpl->set('url', $url);
        $tpl->set('email', $download->Email);
        $tpl->set('code', $download->Code);

        $this->parsedText .= mb_convert_encoding($tpl->parse(), 'CP1251', 'UTF-8');
    }

    /**
     * Get parsed text of template for specified subscriber 
     * and adds block of links for unsubscription.
     * 
     * @param NADDownloadModel $download
     * @return NADTemplateModel
     */
    public function getReadyText(NADDownloadModel $download) {
        $this->_replaceMetaVariables($download);
        $this->_addUnsubscribeBlock($download);
        return $this->parsedText;
    }

}

