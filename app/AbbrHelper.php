<?php

/**
 * Project:   NAD - The Notifications After Download System
 * File:      AbbrHelper.php 
 * Date:      30.03.2012
 * 
 * @package   NAD
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Conformity of RU and EN product's abbreviation.
 *
 * @package   NAD
 * @author    Victor Burak <vb@atompark.com>
 */
class AbbrHelper {

    /**
    * Abbreviation conformity.
    * If abbreviation is identical then RU variant is empty.
    * 
    * @var array [foreign => russian, ...] 
    */
    private static $abbrConformity = array(
        'ams'       => 'mailer',
        'alms'      => 'lmonline',
        'emh'       => 'extractor',
        'aea'       => 'autoresponder',
        'amv'       => 'verifier',
        'eml'       => 'harvester',
        'alm'       => 'listmanager',
        'sm'        => 'subscription',
        'iecspy'    => 'ewebspider',
        'acdee'     => 'cdextractor',
        'awe'       => 'whois',
        'ane'       => 'newsgroupextractor',
        'atldf'     => 'tldfilter',
        'cdrom'     => 'cdrom',
        'epst'      => 'hpst',
        'csv'       => 'hcsv',
        'arc'       => 'harc',
        'mbh'       => 'mailboxhunter',
        'obs'       => 'obs',
        'sms'       => 'sms',
        'studio'    => 'studio',
        'track'     => 'tracker',
        'tracker'   => 'tracker',
        'others'    => 'others',
        'awdb_net'  => 'awdb_net',
        'awdb_biz'  => 'awdb_biz',
        'awdb_info' => 'awdb_info',
        'awdb_org'  => 'awdb_org',
        'awdb_us'   => 'awdb_us',
        'awdb_ru'   => 'awdb_ru',
        'awdb_com'  => 'awdb_com',
        'awdb'      => 'whoisdatabase',
        'blue'      => 'blue',
        'awc'       => 'webcat',
        'dd'        => 'domaincatalogue',
        'survey'    => 'survey',
        'overifier' => 'verifieronline',
        'staffcop'  => 'staffcop',
        'im'        => 'internetmessenger',
        'sthome'    => 'sthome',
        'prsupport' => 'prsupport',
        'etemplate' => 'etemplate',
        'secc'      => 'secc',
        'ale'       => 'leadextractor',
        'apale'     => 'apale',
        'spale'     => 'spale',
        'pfale'     => 'pfale'
    );

    /**
    * Product names in conform to RU abbreviations.
    * 
    * @var array [short_name => full_name, ...] 
    */
    private static $ruProductNames = array(
        'tldfilter'          => 'Maillist TLD Filter',
        'iecspy'             => 'IE Contacts Spy',
        'whois'              => 'Whois Extractor',
        'cdextractor'        => 'CD Extractor',
        'newsgroupextractor' => 'Newsgoup Extractor',
        'verifier'           => 'Verifier',
        'subscription'       => 'Subscription Manager',
        'harvester'          => 'Harvester',
        'listmanager'        => 'List Manager',
        'mailer'             => 'Mailer',
        'extractor'          => 'Extractor',
        'webcat'             => 'Web Catalogue',
        'tracker'            => 'Tracker',
        'mailboxhunter'      => 'Mailbox Hunter для ePochta Harvester',
        'hpst'               => 'PST Plugin для ePochta Harvester',
        'hcsv'               => 'CSV plugin для ePochta Harvester',
        'harc'               => 'Archive Files плугин для ePochta Harvester',
        'addpromo'           => 'AddPromo',
        'epoadd'             => 'Пакет программ ePochta + AddPromo',
        'cmvl'               => 'Client Market',
        'cmvn'               => 'Client Market network',
        'autoresponder'      => 'Autoresponder',
        'blue'               => 'Blue Sender',
        'whoisdatabase'      => 'Whois Database',
        'domaincatalogue'    => 'Domain Catalogue',
        'studio'             => 'Studio',
        'sms'                => 'SMS',
        'icq'                => 'ICQ Sender',
        'survey'             => 'Survey',
        'verifieronline'     => 'Online Verifier',
        'lmonline'           => 'Online List Manager',
        'internetmessenger'  => 'Internet Messenger',
        'ewebspider'         => 'Web Spider',
        'eposem'             => 'Пакет программ ePochta + Semonitor',
        'etemplate'          => 'Разработка email-шаблона',
        'prsupport'          => 'Приоритетная техническая поддержка',
        'leadextractor'      => 'Lead Extractor',
        'apale'              => 'MSN, Yahoo, AIM, ICQ плугин для ePochta Lead Extactor',
        'pfale'              => 'Телефон и факс плагин для ePochta Lead Extractor',
        'spale'              => 'Skype плугин для ePochta Lead Extactor'
    );

    /**
    * Product names in conform to EN abbreviations.
    * 
    * @var array [short_name => full_name, ...] 
    */
    private static $enProductNames = array(
        'ams'       => 'Atomic Mail Sender',
        'alms'      => 'Atomic List Manager Online',
        'emh'       => 'Atomic Email Hunter',
        'aea'       => 'Atomic Email Autoresponder',
        'amv'       => 'Atomic Mail Verifier',
        'eml'       => 'Atomic Email Logger',
        'alm'       => 'Atomic List Manager',
        'sm'        => 'Atomic Subscription Manager',
        'iecspy'    => 'Atomic Web Spider',
        'acdee'     => 'Atomic CD Email Extractor',
        'awe'       => 'Atomic Whois Explorer',
        'ane'       => 'Atomic Newsgroup Explorer',
        'atldf'     => 'Atomic TLD Filter',
        'cdrom'     => 'AtomPark CD',
        'epst'      => 'PST plugin for Atomic Email Logger',
        'csv'       => 'CSV plugin for Atomic Email Logger',
        'arc'       => 'Archives Processing plugin for Atomic Email Logger',
        'mbh'       => 'Mailbox Hunter plug-in for Atomic Email Logger',
        'obs'       => 'Online Backup Service',
        'sms'       => 'Atomic SMS Sender',
        'studio'    => 'Atomic Email Studio',
        'track'     => 'Atomic Email Tracker',
        'others'    => 'Other',
        'awdb_net'  => 'Atomic Whois Database NET Domains',
        'awdb_biz'  => 'Atomic Whois Database BIZ Domains',
        'awdb_info' => 'Atomic Whois Database INFO Domains',
        'awdb_org'  => 'Atomic Whois Database ORG Domains',
        'awdb_us'   => 'Atomic Whois Database US Domains',
        'awdb_ru'   => 'Atomic Whois Database RU Domains',
        'awdb_com'  => 'Atomic Whois Database COM Domains',
        'awdb'      => 'Atomic Whois Database',
        'blue'      => 'Atomic Blue Sender',
        'awc'       => 'Atomic Web Catalogue',
        'dd'        => 'Atomic Domains Catalogue',
        'survey'    => 'Atomic Survey Service',
        'overifier' => 'Atomic Verifier Online',
        'staffcop'  => 'StaffCop Standard',
        'im'        => 'Atomic Internet Messenger',
        'sthome'    => 'StaffCop Home Edition',
        'prsupport' => 'Priority technical support',
        'etemplate' => 'Email template development',
        'secc'      => 'Security Curator',
        'ale'       => 'Atomic Lead Extractor',
        'apale'     => 'MSN, Yahoo, AIM, ICQ plugins for Atomic Lead Extactor',
        'spale'     => 'Skype plugin for Atomic Lead Extractor',
        'pfale'     => 'Phone and Fax plugins for Atomic Lead Extractor'
    );

    /**
     * Checks whether is RU abbreviation.
     * 
     * @param  string $abbr
     * @return string
     */
    public static function isRuAbbr($abbr) {
        return array_search($abbr, self::$abbrConformity) ? TRUE : FALSE;
    }

    /**
     * Checks whether is EN abbreviation.
     * 
     * @param  string $abbr
     * @return string
     */
    public static function isEnAbbr($abbr) {
        return array_key_exists($abbr, self::$abbrConformity) ? TRUE : FALSE;
    }

    /**
     * Get full name of product for RU abbreviation.
     * 
     * @param  string $abbr
     * @return string
     */
    public static function getRuName($abbr) {
        return self::isRuAbbr($abbr) ? self::$ruProductNames[$abbr] 
                                     : self::$ruProductNames[self::getAnalog($abbr)];
    }

    /**
     * Get full name of product for EN abbreviation.
     * 
     * @param  string $abbr
     * @return string
     */
    public static function getEnName($abbr) {
        return self::isEnAbbr($abbr) ? self::$enProductNames[$abbr] 
                                     : self::$enProductNames[self::getAnalog($abbr)];
    }
    
    /**
     * Convert abbreviation from EN to RU.
     * 
     * @param  string $abbr
     * @return string
     */
    public static function getRuAbbr($abbr) {
        return array_key_exists($abbr, self::$abbrConformity) ? self::$abbrConformity[$abbr] : $abbr;
    }
    
    /**
     * Convert abbreviation from RU to EN.
     * 
     * @param  string $abbr
     * @return string
     */
    public static function getEnAbbr($abbr) {
        if ($key = array_search($abbr, self::$abbrConformity)) {
            return $key;
        } else {
            return $abbr;
        }
    }

    public static function getAnalog($abbr) {
        return self::isEnAbbr($abbr) ? self::getRuAbbr($abbr) : self::getEnAbbr($abbr);
    }
}
