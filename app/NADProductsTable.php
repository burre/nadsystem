<?php

/**
 * Project:   NAD - The Notifications After Download System
 * File:      NADProductsTable.php 
 * Date:      21.03.2012
 * 
 * @package   NAD
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * NADProductsTable
 *
 * @uses      BDbTable
 * @package   NAD 
 * @author    Victor Burak <vb@atompark.com>
 */
class NADProductsTable extends BDbTable {

    /**
     * Class constructor.
     * 
     * @return void 
     */
    public function __construct() {
        $this->setAdapter(NADSystem::getRegistry()->get('DB2'));
        $this->setTableName(NADSystem::getRegistry()->get('table.products.name'));
        $this->setPrimaryKey(NADSystem::getRegistry()->get('table.products.pkey'));

        parent::__construct();
        
    }

}
