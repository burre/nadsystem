<?php

/**
 * Project:   NAD - The Notifications After Download System
 * File:      NADSystem.php 
 * Date:      05.03.2012
 * 
 * @package   NAD
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * NAD - The Notifications After Download System
 * 
 * The main application class.
 *
 * @package   NAD 
 * @author    Victor Burak <vb@atompark.com>
 */
class NADSystem {

    /**
     * Class information (used in __toString()).
     * 
     * @var string 
     */
    private static $_info = 'NADSystem Class singletone instanse';

    /**
     * The NADSystem Class instance.
     * 
     * @var NADSystem
     */
    private static $_instance;

    /**
     * Application registy.
     * 
     * @var BRegistry
     */
    private static $_registry;

    /**
     * Application config file.
     * 
     * @var string
     */
    private static $_config_file;

    // SINGLETONE PART

    /**
     * Create NADSystem application object. 
     * 
     * Made 'private' to prevent a direct instantiation.
     * 
     * @param string $config_file
     */
    private function __construct() {
        self::$_registry = new BRegistry();
        
        // set application's variables from config file
        self::getRegistry()->addArray(BHelpers::parseConfig(self::$_config_file));
    }
    
    /**
     * Initialize application.
     * 
     * @return void 
     */
    public function init() {
        // initialize and store system database adapter
        $dbCredentials = array(
            'dbHost' => self::getRegistry()->get('db.host'),
            'dbUser' => self::getRegistry()->get('db.user'),
            'dbPass' => self::getRegistry()->get('db.pass'),
            'dbName' => self::getRegistry()->get('db.name'),
            'dbPort' => self::getRegistry()->get('db.port'),
        );
//        self::getRegistry()->set('DB', BDatabaseS::getInstance($dbCredentials));
        
//        $db1 = new BDatabaseNS($dbCredentials, 'UTF8');
//        $db2 = new BDatabaseNS($dbCredentials, 'LATIN1');
        
        self::getRegistry()->set('DB1', new BDatabaseNS($dbCredentials, 'UTF8'));
        self::getRegistry()->set('DB2', new BDatabaseNS($dbCredentials, 'LATIN1'));
        
        // initialize and store system SMTP mailer
        $smtpCredentials = array(
            'smtpHost' => self::getRegistry()->get('smtp.host'),
            'smtpUser' => self::getRegistry()->get('smtp.user'),
            'smtpPass' => self::getRegistry()->get('smtp.pass'),
            'smtpPort' => self::getRegistry()->get('smtp.port'),
        );
        self::getRegistry()->set('MAIL', new BMail($smtpCredentials));
     
        // initialize and store system logger
        $logFileName = str_replace('%DATE%', date('d_m_Y'), self::getRegistry()->get('log.filename'));
        $logFileName = self::getRegistry()->get('LOGPATH') . $logFileName;
        self::getRegistry()->set('LOG', new BFileLogger($logFileName));

        // store request variables
        self::getRegistry()->set('REQUEST', new BRequest());
    }

    /**
     *  Made 'private' to prevent cloning the object.
     */
    private function __clone() {
        
    }

    /**
     *  Made 'private' to prevent deserializing the object.
     */
    private function __wakeup() {
        
    }

    /**
     * Print object summary.
     * 
     * @return string
     */
    public function __toString() {
        return self::$_info;
    }

    /**
     * Get application registry.
     * 
     * @return BRegistry
     */
    public static function getRegistry() {
        return self::$_registry;
    }

    /**
     * Some magic methods.
     * 
     * @param  string $name
     * @param  mixed $arguments
     * @return mixed 
     */
    public function __call($name, $arguments) {
        
    }

    /**
     * Get instance of 'singletone'.
     * 
     * @return NADSystem
     */
    public static function getInstance($config_file = '') {
        self::$_config_file = $config_file;
        if (!isset(self::$_instance)) {
            $class_name = __CLASS__;
            self::$_instance = new $class_name;
        }
        return self::$_instance;
    }

    // END OF SINGLETONE PART

    /**
     * Run the NADSystem application.
     * 
     * Perform go-round all of templates and send messages 
     * for all subscribers of each template.
     * 
     * @return void
     */
    public function run() {
        /* @var $logger BFileLogger */
        $logger               = self::getRegistry()->get('LOG');
        $totalSentCounter     = 0;
        $totalBadEmailCounter = 0;

        $template             = new NADTemplateModel();
        $tplCollect           = $template->getAllActive();
        
        $logger->write('РАССЫЛКА ВСЕМ ПОДПИСЧИКАМ');
        $logger->write('Всего активных шаблонов: ' . count($tplCollect));
        
        // loop through the collection of templates
        foreach ($tplCollect as $template) {
            // get the subscribers of current template
            $download   = new NADDownloadModel();
            $dwnCollect = $download->getAllSubscribers($template);
            
            $abbrAnalog = AbbrHelper::getAnalog($template->abbr);
            $logger->write("- Шаблон: id #{$template->id}, тип: {$template->type}, программа: {$template->abbr} ({$abbrAnalog}), язык: {$template->lang}");
            $logger->write('    Подписчиков: ' . count($dwnCollect) . ' без фильтра по языку');
            
            // loop through the collection of template's subscribers
            if (count($dwnCollect) > 0) {
                $sentCounter     = 0;
                $badEmailCounter = 0;
                $logger->write('    Отправляем письма:');
                
                foreach ($dwnCollect as $download) {
                    if (BHelpers::isValidEmail($download->Email)) {
                        if ($download->langMatchesTemplate($template)) {
                            // check for new orders for last 10 days is template type is 10
                            if ($template->type == 10 AND $download->hasOrderedProductsForLast10Days()) {
                                continue;
                            }
                            
                            if (!$download->alreadySent($template)) {
                                @$logger->write("        -> {$download->Email}, закачка: {$download->DTime}");
                                $download->sendMail($template);
                                $sentCounter++;
                                $totalSentCounter++;
                            }
                        }
                    } else {
                        $badEmailCounter++;
                        $totalBadEmailCounter++;
                        @$logger->write("        -- неверный адрес: {$download->Email}, закачка: {$download->DTime}, запись #{$download->ID}");
                    }
                }
                
                $logger->write("    Отправлено писем: {$sentCounter}, неверных адресов: {$badEmailCounter}");
            }
        }
        
        $logger->write("------------------------------------------------------------");
        $logger->write("Всего отправлено писем: {$totalSentCounter}, всего неверных адресов: {$totalBadEmailCounter}");
    }

    /**
     * Send first email to specified downloader.
     * 
     * @param  integer $id 
     * @return void
     */
    public function sendFirstMail($id) {
        if ($id) {
            $template     = new NADTemplateModel();
            $download     = new NADDownloadModel();
            $download->ID = $id;
            $download->load();
            $lang = $download->detectLang();

            if ($template->getOneByATL($download->Program, 1, $lang)
                OR $template->getOneByATL(AbbrHelper::getAnalog($download->Program), 1, $lang)) {
                $download->sendMail($template);

                /* @var $logger BFileLogger */
                $logger = NADSystem::getRegistry()->get('LOG');
                $logger->write("- Отправлено первое письмо: {$this->Email}, программа {$download->Program}, язык {$lang}.");
            }
        }
    }
    
    /**
     * Some routines before application's shutdown. 
     * 
     * @return void
     */
    public function beforeExit() {
        self::getRegistry()->get('DB1')->close();
        self::getRegistry()->get('DB2')->close();
        self::getRegistry()->get('LOG')->close();
    }
    
    /**
     * Unsubscribe via the web page.
     * 
     * @return void 
     */
    public function unsubscribe() {
        $unsubController = new NADUnsubscribeController();
        $unsubController->route();
    }
    
}
