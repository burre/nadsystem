<?php

/**
 * Project:   NAD - The Notifications After Download System
 * File:      NADTemplateCollection.php 
 * Date:      19.03.2012
 * 
 * @package   NAD
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Collection of NADTemplateModel objects.
 *
 * @uses      BCollection
 * @package   NAD 
 * @author    Victor Burak <vb@atompark.com>
 */
class NADTemplateCollection extends BCollectionBase {

    /**
     * Class constructor.
     * 
     * @return void 
     */
    public function __construct() {
        parent::__construct('NADTemplateModel');
    }

}
