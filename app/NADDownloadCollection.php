<?php

/**
 * Project:   NAD - The Notifications After Download System
 * File:      NADDownloadCollection.php 
 * Date:      21.03.2012
 * 
 * @package   NAD
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of NADDownloadCollection
 *
 * @uses      BCollection
 * @package   NAD 
 * @author    Victor Burak <vb@atompark.com>
 */
class NADDownloadCollection extends BCollectionBase {

    /**
     * Class constructor.
     * 
     * @return void 
     */
    public function __construct() {
        parent::__construct('NADDownloadModel');
    }

}
