<?php

/**
 * Project:   NAD - The Notifications After Download System
 * File:      NADTemplatesTable.php 
 * Date:      14.03.2012
 * 
 * @package   NAD
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Database table 'nad_templates'.
 *
 * @uses      BDbTable
 * @package   NAD 
 * @author    Victor Burak <vb@atompark.com>
 */

class NADTemplatesTable extends BDbTable {

    /**
     * Class constructor.
     * 
     * @return void 
     */
    public function __construct() {
        $this->setAdapter(NADSystem::getRegistry()->get('DB2'));
        $this->setTableName(NADSystem::getRegistry()->get('table.templates.name'));
        $this->setPrimaryKey(NADSystem::getRegistry()->get('table.templates.pkey'));

        parent::__construct();
    }

}
