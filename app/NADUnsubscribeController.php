<?php

/**
 * Project:   NAD - The Notifications After Download System
 * File:      NADUnsubscribeController.php 
 * Date:      29.03.2012
 * 
 * @package   NAD
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * NAD unsubscription controller.
 *
 * @package   NAD
 * @author    Victor Burak <vb@atompark.com>
 */
class NADUnsubscribeController {

    /**
     * Represent system request object.
     * 
     * @var BRequest
     */
    private $_request;

    /**
     * Represent the $_GET array.
     * 
     * @var array 
     */
    private $_get;

    /**
     * Represent the $_POST array.
     * 
     * @var array
     */
    private $_post;

    /**
     * Template object for this controller.
     * 
     * @var BTemplate 
     */
    private $_tpl;

    /**
     * Class constructor.
     * 
     * Prepares request variables and initializes template.
     * 
     * @return void
     */
    public function __construct() {
        $this->_request = NADSystem::getRegistry()->get('REQUEST');
        $this->_get     = BHelpers::clearUserInputArray($this->_request->get());
        $this->_post    = BHelpers::clearUserInputArray($this->_request->post());

        $this->_tpl     = new BTemplate();
        $this->_tpl->setDir(NADSystem::getRegistry()->get('TPLPATH'));
    }

    /**
     * Simple routing for requests.
     * 
     * @return void
     */
    public function route() {
        if ($this->_request->isPost()) {
            if (isset($this->_get, $this->_get['e'], $this->_get['c'])) {
                // unsubscribe via link within regular email (after variant selecting)
                $this->doUnsubscribe();
            } elseif(isset($this->_post['email'])) {
                // unsubscribe by email only via web-form
                $this->indexAction($this->_post['email']);
            }
        } else {
            if (isset($this->_get, $this->_get['e'], $this->_get['c'])) {
                if (isset($this->_get['atonce'])) {
                    // unsubscribe via link within confirmation email
                    $this->doUnsubscribe('all');
                } else {
                    // unsubscribe via link within regular email
                    $this->confirmAction();
                }
            } else {
                // index page - unsubscribe by email only via web-form
                $this->indexAction();
            }
        }
    }

    /**
     * Start page of manual unsubscription procedure.
     * 
     * This action handles the form on start page. 
     * If $email contains a valid email address and user is within our database,
     * we'll send a confirmation letter with the direct link for unsubscribing 
     * bypassing the form with allowed variants of unsubscription.
     * 
     * @param string $email e.g. 'name@host.com'
     * @return void 
     */
    public function indexAction($email = '') {
        $lang = NADSystem::getRegistry()->get('lang');
        if ($email) {
            $download    = new NADDownloadModel();
            $tplFileName = 'unsub_end_' . strtolower($lang) . '.phtml';
            $this->_tpl->setFile($tplFileName);
            
            if ($download->getOneByEmail($email)) {
                $download->sendConfirmationMail();
                
                if ($lang == 'EN') {
                    $successMessage = 'Check your email for a message with the link for unsubscribing.';
                } elseif ($lang == 'RU') {
                    $successMessage = 'На указанный вами адрес отправлено письмо со ссылкой на удаление подписки.';
                } elseif ($lang == 'ES') {
                    $successMessage = 'Revise su correo electrónico un mensaje con el enlace para darse de baja.';
                }

                $this->_tpl->set('successMessage', $successMessage);
                
            } else {
                if ($lang == 'EN') {
                    $errMessage = "Subscription not found for this e-mail: {$email}.";
                } elseif ($lang == 'RU') {
                    $errMessage = "Не найдено подписок для адреса: {$email}.";
                } elseif ($lang == 'ES') {
                    $errMessage = "Suscripción que no se encuentra de este e-mail: {$email}.";
                }

                $this->_tpl->set('errMessage', $errMessage);
            }
        } else {
            $tplFileName = 'index_' . strtolower($lang) . '.phtml';
            $this->_tpl->setFile($tplFileName);
        }
        
        $this->_tpl->render();
    }
    
    /**
     * Shows form with variants of unsubscription or
     * error message if subscriber not found.
     * 
     *  @return void
     */
    public function confirmAction() {
        $download = new NADDownloadModel();
        
        if ($download->getOneByEmailAndCode($this->_get['e'], $this->_get['c'])) {
            $lang        = $download->detectLang();
            $tplFileName = 'disc_block_' . strtolower($lang) . '.phtml';
            $this->_tpl->setFile($tplFileName);
            $discountBlock = $this->_tpl->parse();
            
            $progName = ($lang == 'RU')
                      ? AbbrHelper::getRuName($download->Program)
                      : AbbrHelper::getEnName($download->Program); 
            
            $tplFileName = 'unsub_start_' . strtolower($lang) . '.phtml';
            $this->_tpl->setFile($tplFileName);
            $this->_tpl->set('name', $download->firstname . ' ' . $download->lastname);
            $this->_tpl->set('email', $download->Email);
            $this->_tpl->set('program', $progName);
            $this->_tpl->set('discountBlock', $discountBlock);
        } else {
            $lang = NADSystem::getRegistry()->get('lang');
            
            if ($lang == 'EN') {
                $errMessage = "Subscription not found for this e-mail: {$this->_get['e']}.";
            } elseif ($lang == 'RU') {
                $errMessage = "Не найдено подписок для адреса: {$this->_get['e']}.";
            } elseif ($lang == 'ES') {
                $errMessage = "Suscripción que no se encuentra de este e-mail: {$this->_get['e']}.";
            }
            
            $tplFileName = 'unsub_start_' . strtolower($lang) . '.phtml';
            $this->_tpl->setFile($tplFileName);
            $this->_tpl->set('errMessage', $errMessage);
        }
        
        $this->_tpl->render();
    }

    /**
     * Do the unsubscription procedure after form sending. 
     * 
     * @param string $mode 'one' or 'all' are valid values<br />
     * It $mode is empty - his value will be taken from POST variable `subscription'.
     * @return void
     */
    public function doUnsubscribe($mode = '') {
        $download = new NADDownloadModel();
        
        if (empty($mode)) {
            if (isset($this->_post['subscription']) AND $this->_post['subscription'] == 'one') {
                $mode = 'one';
            } elseif (isset($this->_post['subscription']) AND $this->_post['subscription'] == 'all') {
                $mode = 'all';
            }
        }
        
        if ($download->getOneByEmailAndCode($this->_get['e'], $this->_get['c'])) {
            $lang = $download->detectLang();
            $tplFileName = 'unsub_end_' . strtolower($lang) . '.phtml';
            $this->_tpl->setFile($tplFileName);

            if ($lang == 'EN') {
                $successMessage = 'You are successfully unsubscribed.';
                $errMessage = 'An error happened while unsubscribe attempt.';
            } elseif ($lang == 'RU') {
                $successMessage = 'Вы успешно отписались.';
                $errMessage = 'При попытке отписаться произошла ошибка.';
            } elseif ($lang == 'ES') {
                $successMessage = 'Usted se baja satisfactoriamente.';
                $errMessage = 'Un error ocurrió mientras intento de darse de baja.';
            }
            
            if ($mode == 'one') {
                if ($download->unsubscribeOne()) {
                    $this->_tpl->set('errMessage', $successMessage);
                } else {
                    $this->_tpl->set('errMessage', $errMessage);
                }
            } elseif ($mode == 'all') {
                if ($download->unsubscribeAll()) {
                    $this->_tpl->set('errMessage', $successMessage);
                } else {
                    $this->_tpl->set('errMessage', $errMessage);
                }
            }
            
            $this->_tpl->render();
        }
    }
    
}
