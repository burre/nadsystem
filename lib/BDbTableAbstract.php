<?php

/**
 * Project:   BTools
 * File:      BDbTableAbstract.php 
 * Date:      20.11.2011
 * 
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Database table abstact class.
 *
 * @package   BTools 
 * @author    Victor Burak <vb@atompark.com>
 */

abstract class BDbTableAbstract {

    const DEFAULT_ADAPTER = 'DB';

    /**
     * Database adapter.
     * @var mixed
     */
    private $_adapter;

    /**
     *Class constructor.
     *  
     */
    abstract public function __construct();

    /**
     * Set database adapter.
     * @param BDatabaseInterface $adapter
     * @return BDbTableAbstract
     */
    protected function setAdapter(BDatabaseInterface $adapter) {
        $this->_adapter = $adapter;
        return $this;
    }

    /**
     * Get current adapter
     * @return BDatabaseInterface
     */
    protected function getAdapter() {
        return $this->_adapter;
    }

}