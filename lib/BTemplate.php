<?php

/**
 * Project:   BTools
 * File:      BTemplate.php 
 * Date:      20.11.2011
 * 
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Template class.
 *
 * @package   BTools 
 * @author    Victor Burak <vb@atompark.com>
 */
class BTemplate {

    /**
     * Directory name where templates are stored.
     * @var string
     */
    private $_dir;

    /**
     * Current template file name.
     * @var string
     */
    private $_file;

    /**
     * Template vars.
     * @var array
     */
    private $_vars = array();

    /**
     * Create new Template object.
     * @return void
     */
    function __construct($file = '') {
        if (!empty($file)) {
            $this->setFile($file);
        }
    }

    /**
     * Parses a template file and returns complete code.
     * @param string $file
     * @return string
     */
    public function parse() {
        ob_start();
        extract($this->_vars);
        require($this->_dir . $this->_file);
        return ob_get_clean();
    }

    /**
     * Render a template file.
     * @param string $file
     * @return void
     */
    public function render() {
        echo $this->parse();
    }

    /**
     * Set a template variable.
     * @param string $name
     * @param mixed $val
     * @return \BTemplate 
     */
    public function set($name, $val) {
        if (!empty($name)) {
            $this->_vars[$name] = $val;
        }
        return $this;
    }

    /**
     * Set template file.
     * @param string $file
     * @return BTemplate | boolean
     * @throws Exception 
     */
    public function setFile($file) {
        if (!empty($file) AND file_exists($this->_dir . $file)) {
            $this->_file = $file;
            return $this;
        } else {
            throw new Exception('BTemplate: File does not exists!');
            return FALSE;
        }
    }

    /**
     * Sets directory path where templates are stored.
     * @param string $dir
     * @return BTemplate | boolean
     * @throws Exception 
     */
    public function setDir($dir) {
        if (!empty($dir) AND file_exists($dir)) {
            $this->_dir = $dir;
            return $this;
        } else {
            throw new Exception('BTemplate: Directory does not exists!');
            return FALSE;
        }
    }

    /**
     * Clear a certain template variable or whole array.
     * @param string $var | none
     * @return void
     */
    public function clear($var = NULL) {
        if (isset($var) && array_key_exists($var, $this->_vars)) {
            unset($this->_vars[$var]);
        } else {
            $this->_vars = array();
        }
    }

}