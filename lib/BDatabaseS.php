<?php

/**
 * Project:   BTools
 * File:      BDatabaseS.php 
 * Date:      20.11.2011
 * Updated:   17.04.2012
 * 
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Database adapter class (singletone).
 *
 * @uses      mysqli
 * @package   BTools 
 * @author    Victor Burak <vb@atompark.com>
 */

final class BDatabaseS extends BDatabaseBase {
    
    public $info = 'BDatabaseS (Singletone)';

    /**
     * The Database Class instance.
     * @var BDatabaseS
     */
    private static $_instance;

    /**
     * Create Database object and establish connection.
     * @param array $credentials
     * @return BDatabaseS
     */
    private function __construct($credentials) {
        if ($credentials && is_array($credentials)) {
            !($this->_credentials = new stdClass()) OR
                    $this->_credentials = (object) $credentials;
        } else {
            throw new Exception('Error! No credentials provided for a database access.');
        }

        if (!$this->_connection) {
            $this->_connection = parent::__construct(
                    $this->_credentials->dbHost, 
                    $this->_credentials->dbUser, 
                    $this->_credentials->dbPass, 
                    $this->_credentials->dbName, 
                    $this->_credentials->dbPort
            );

            if ($this->connect_errno) {
                throw new Exception("Connection Error $this->connect_errno: $this->connect_error");
            } else {
                $this->_query = new stdClass();
                $this->_query->sql = '';
                $this->_query->success = NULL;
                $this->_query->start_time = 0;
                $this->_query->end_time = 0;
                $this->_query->spent_time = 0;
            }
        }
    }

    /**
     *  Made 'private'  to prevent cloning an object.
     */
    private function __clone() {
        
    }

    /**
     * Get instance of 'singletone'.
     * 
     * @param array $credentials
     * @return BDatabaseS
     * @throws Exception 
     */
    public static function getInstance($credentials = NULL) {
        if (!isset(self::$_instance)) {
            if (!$credentials) {
                throw new Exception('Error: No database credentials provided!');
            }
            self::$_instance = new self($credentials);
        }
        return self::$_instance;
    }

}