<?php

/**
 * Project:   BTools
 * File:      BCollection.php 
 * Date:      15.03.2012
 * 
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Base class for any object collections.
 *
 * @uses      SPL - IteratorAggregate, ArrayAccess, Countable
 * @package   BTools 
 * @author    Victor Burak <vb@atompark.com>
 */
class BCollectionBase implements IteratorAggregate, ArrayAccess, Countable {

    /**
     * Type of objects in collection.
     * 
     * @var string 
     */
    private $_type;

    /**
     * Objects container.
     * 
     * @var array 
     */
    private $_collection = array();

    /**
     * Class constructor.
     * 
     * @param  string $type Class name of objects that are supposed to store.
     * @throws Exception 
     * @return void
     */
    public function __construct($type) {
        if (!empty($type)) {
            $this->_type = $type;
        } else {
            throw new Exception('BCollection: Invalid type definition!');
        }
    }

    /**
     * Add object to collection.
     * 
     * @param  mixed $object
     * @return BCollectionBase 
     */
    public function add($object) {
        $this->_checkType($object);
        $this->_collection[] = $object;
        return $this;
    }
    
    /**
     * Remove object from collection.
     * 
     * @param mixed $object
     * @return BCollectionBase 
     */
    public function remove($object) {
        $this->_checkType($object);
        unset($this->_collection[array_search($object, $this->_collection)]);
        return $this;
    }
    
    /**
     * Clear the collection.
     * 
     * @return BCollectionBase 
     */
    public function clear() {
        $this->_collection = array();
        return $this;
    }

    /**
     * Get collection type.
     * 
     * @return string 
     */
    public function getType() {
        return $this->_type;
    }

    /**
     * Checks whether is empty the collection.
     * 
     * @return type 
     */
    public function isEmpty() {
        return empty($this->_collection);
    }    
    
    /**
     * Checks type conformity.
     * 
     * @param  mixed $object
     * @throws Exception 
     * @return void
     */
    private function _checkType($object) {
        if (!$object instanceof $this->_type)
            throw new Exception('BCollection: type mismatch while adding object!');
    }

    /**
     * Implementation of IteratorAggregate.
     * 
     * @return ArrayIterator 
     */
    public function getIterator() {
        return new ArrayIterator($this->_collection);
    }

    /**
     * Implementation of ArrayAccess.
     * 
     * @param  integer $offset
     * @param  integer $value 
     * @return void
     */
    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->_collection[] = $value;
        } else {
            $this->_collection[$offset] = $value;
        }
    }

    /**
     * Implementation of ArrayAccess.
     * 
     * @param  integer $offset
     * @return boolean
     */
    public function offsetExists($offset) {
        return isset($this->_collection[$offset]);
    }

    /**
     * Implementation of ArrayAccess.
     * 
     * @param  integer $offset 
     * @return void
     */
    public function offsetUnset($offset) {
        unset($this->_collection[$offset]);
    }

    /**
     * Implementation of ArrayAccess.
     * @param  integer $offset
     * @return mixed
     */
    public function offsetGet($offset) {
        return isset($this->_collection[$offset]) ? $this->_collection[$offset] : null;
    }

    /**
     * Implementation of Countable.
     * 
     * @return integer
     */
    public function count() {
        return count($this->_collection);
    }

}