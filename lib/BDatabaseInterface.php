<?php

/**
 * Project:   NAD
 * File:      BDatabaseInterface.php 
 * Date:      17 квіт 2012
 * 
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Database adapter interface.
 *
 * @package   BTools 
 * @author    Victor Burak <vb@atompark.com>
 */
interface BDatabaseInterface {

    public function query($query);

    public function getRowAsArray();

    public function getRowAsObject();

    public function getRowsetAsArray();

    public function close();

    public function getAffectedRows();

    public function freeLastResult();
}
