<?php

/**
 * Project:   BTools
 * File:      BModelAbstract.php 
 * Date:      20.11.2011
 * 
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Data model abstract class.
 *
 * @uses      BModelInterface
 * @package   BTools 
 * @author    Victor Burak <vb@atompark.com>
 */
abstract class BModelAbstract implements BModelInterface {

    /**
     * BDbTable object.
     * @var BDbTable
     */
    private $_db_table;

    /**
     * Model data.
     * @var stdClass
     */
    private $_model_data;

    /**
     * Fields that are not listed in the BDbTable.
     * @var array
     */
    private $_extra_fields = array();

    /**
     * Extra data - propertie that are not directly mapped into BDbTable.
     * @var stdClass
     */
    private $_extra_data = array();

    /**
     *Class constructor.
     *  
     */
    abstract public function __construct();

    /**
     * Overloading for quick field access.
     * @param string $name
     * @return mixed
     */
    public function __get($name) {
        if ($this->getDbTable()->hasField($name)) {
            return $this->_model_data->$name;
        } elseif ($this->hasExtraField($name)) {
            return $this->getExtraField($name);
        } else {
            return NULL;
        }
    }

    /**
     * Overloading for quick field updating.
     * @param string $name
     * @param mixed $name
     */
    public function __set($name, $value) {
        if ($this->getDbTable()->hasField($name)) {
            $this->_model_data->$name = $value;
        } elseif ($this->hasExtraField($name)) {
            $this->setExtraField($name, $value);
        }
    }

    /**
     * Initialise model data object.
     * All data previously loaded into model will be cleared.
     * 
     * @return ModelAbstract
     */
    public function initModelData() {
        $this->_model_data = new stdClass();

        $fields = $this->getDbTable()->getFieldList();
        foreach ($fields as $field) {
            $this->_model_data->$field = NULL;
        }

        $fields = $this->getExtraFieldsList();
        foreach ($fields as $field) {
            $this->_model_data->$field = NULL;
        }

        return $this;
    }

    /**
     * Get the BDbTable object.
     * @return BDbTable
     */
    protected function getDbTable() {
        return $this->_db_table;
    }

    /**
     * Set the BDbTable object.
     * @return BModelAbstract
     */
    protected function setDbTable(BDbTable $db_table) {
        $this->_db_table = $db_table;
        return $this;
    }

    /**
     * Get the extra fields list.
     * @return array
     */
    protected function getExtraFieldsList() {
        return $this->_extra_fields;
    }

    /**
     * Set the extra fields list.
     * @param array $fields
     * @return ModelAbstract
     */
    protected function setExtraFieldsList($fields) {
        $this->_extra_fields = $fields;
        return $this;
    }

    /**
     * Get the extra field data.
     * @param string $name
     * @return mixed
     */
    protected function getExtraField($name) {
        return $this->_extra_data[$name];
    }

    /**
     * Set the extra field data.
     * @param string $name
     * @param mixed $value
     * @return ModelAbstract
     */
    public function setExtraField($name, $value) {
        $this->_extra_data[$name] = $value;
        return $this;
    }

    /**
     * Check if the table has a extra field.
     * @param string $name
     * @return bool
     */
    public function hasExtraField($name) {
        return in_array($name, $this->_extra_fields);
    }

    /**
     * Load model data.
     * @TODO UGLY METHOD
     * @param string $where
     * @return UserModel|FALSE on error
     */
    public function load($where = '') {
        if (!$where) {
            $idField = $this->getDbTable()->getPrimaryKey();
            if ($this->$idField) {
                if ($result = $this->getDbTable()->findByPrimaryKey($this->$idField)) {
                    $this->setModelData($result[0]);
                } else {
                    $this->initModelData();
                }
            } else {
                throw new Exception('Error: No primary key presented for select data!');
                return FALSE;
            }
        } else {
            
        }

        return $this;
    }

    public function create() {
        if ($new_id = $this->getDbTable()->insert((array) $this->_model_data))
            $this->id = $new_id;

        return $this;
    }

    public function delete() {
        if ($this->getDbTable()->delete($this->id)) {
            $this->initModelData();
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function save() {
        if ($this->getDbTable()->update((array) $this->_model_data))
            return TRUE;
        else
            return FALSE;
    }

    /**
     * Set the model's data from associative array (e.g query result).
     * @param array $array 
     * @return BModelAbstract
     */
    protected function setModelData($array) {
        $this->_model_data = (object) $array;
        return $this;
    }
    
    
    /**
     * Returns the model data as array (default) or
     * if $mode is TRUE - as stdObject.
     * 
     * @param boolean $mode
     * @return mixed array | stdObject
     */
    public function getModelData($mode = FALSE) {
        return $mode ? $this->_model_data : (array) $this->_model_data;
    }

}