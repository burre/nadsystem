<?php

/**
 * Project:   BTools
 * File:      BRequest.php 
 * Date:      11.01.2012
 * 
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Request class.
 *
 * @package   BTools 
 * @author    Victor Burak <vb@atompark.com>
 */

class BRequest {

    /**
     * Data hash.
     * @var array
     */
    private $_hash = array();

    /**
     * Create the new BRequest object.
     * @return void
     */
    public function __construct() {
        $this->_hash['get'] = $_GET;
        $this->_hash['post'] = $_POST;
        $this->_hash['cookie'] = $_COOKIE;
        $this->_hash['request'] = $_REQUEST;
        $this->_hash['server'] = $_SERVER;
        $this->_hash['session'] = $_SESSION;
        $this->_hash['files'] = $_FILES;
    }

    /**
     * GET data.
     * @param string $var
     * @return array|mixed Array or concrete value. 
     */
    public function get($var = NULL) {
        if ($var == NULL) {
            return $this->_hash['get'];
        } else {
            return isset($this->_hash['get'][$var]) ? $this->_hash['get'][$var] : NULL;
        }
    }

    /**
     * POST data.
     * @param string $var
     * @return array|mixed Array or concrete value. 
     */
    public function post($var = NULL) {
        if ($var == NULL) {
            return $this->_hash['post'];
        } else {
            return isset($this->_hash['post'][$var]) ? $this->_hash['post'][$var] : NULL;
        }
    }

    /**
     * COOKIE data.
     * @param string $var
     * @return array|mixed Array or concrete value. 
     */
    public function cookie($var = NULL) {
        if ($var == NULL) {
            return $this->_hash['cookie'];
        } else {
            return isset($this->_hash['cookie'][$var]) ? $this->_hash['cookie'][$var] : NULL;
        }
    }

    /**
     * REQUEST data.
     * @param string $var
     * @return array|mixed Array or concrete value. 
     */
    public function request($var = NULL) {
        if ($var == NULL) {
            return $this->_hash['request'];
        } else {
            return isset($this->_hash['request'][$var]) ? $this->_hash['request'][$var] : NULL;
        }
    }

    /**
     * SERVER data.
     * @param string $var
     * @return array|mixed Array or concrete value. 
     */
    public function server($var = NULL) {
        if ($var == NULL) {
            return $this->_hash['server'];
        } else {
            return isset($this->_hash['server'][$var]) ? $this->_hash['server'][$var] : NULL;
        }
    }

    /**
     * SESSION data.
     * @param string $var
     * @return array|mixed Array or concrete value. 
     */
    public function session($var = NULL) {
        if ($var == NULL) {
            return $this->_hash['session'];
        } else {
            return isset($this->_hash['session'][$var]) ? $this->_hash['session'][$var] : NULL;
        }
    }

    /**
     * FILES data.
     * @param string $var
     * @return array|mixed Array or concrete value. 
     */
    public function files($var = NULL) {
        if ($var == NULL) {
            return $this->_hash['files'];
        } else {
            return isset($this->_hash['files'][$var]) ? $this->_hash['files'][$var] : NULL;
        }
    }

    /**
     * Remote IP address.
     * @return string 
     */
    public function ip() {
        return $this->server('REMOTE_ADDR');
    }

    /**
     * Request URI.
     * @return string 
     */
    public function uri() {
        return $this->server('REQUEST_URI');
    }

    /**
     * Host name.
     * @return string
     */
    public function host() {
        return $this->server('HTTP_HOST');
    }

    /**
     * HTTP protocol.
     * @return string 
     */
    public function protocol() {
        return 'http://';
    }

    /**
     * Whole URL.
     * @return string
     */
    public function url() {
        return $this->protocol() . $this->host() . $this->uri();
    }

    /**
     * HTTP referer.
     * @return string 
     */
    public function referer() {
        return $this->server('HTTP_REFERER');
    }

    /**
     * Is last request a POST?
     * @return bool 
     */
    public function isPost() {
        return isset($this->_hash['post'])
                && !empty($this->_hash['post']);
    }

    /**
     * Is last request a AJAX?
     * @return bool 
     */
    public function isAjax() {
        $xhr = $this->server('HTTP_X_REQUESTED_WITH');
        return isset($xhr) && $xhr === 'XMLHttpRequest';
    }

}