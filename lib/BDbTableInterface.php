<?php

/**
 * Project:   BTools
 * File:      BDbTableInterface.php 
 * Date:      20.11.2011
 * 
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Database table interface.
 *
 * @package   BTools 
 * @author    Victor Burak <vb@atompark.com>
 */

interface BDbTableInterface {

    public function insert(array $data);

    public function delete($id);

    public function select($where, $order = NULL, $limit = NULL);

    public function update(array $data);
}