<?php

/**
 * Project:   NAD - The Notifications After Download System
 * File:      NAD.bootstrap.php 
 * Date:      05.03.2012
 * 
 * @package   NAD
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Bootstrap file for NAD - Notificetions After Download System.
 *
 * @package   NAD 
 * @author    Victor Burak <vb@atompark.com>
 */

/**
 * Set the default time zone, because the server's time may be varied. 
 */
date_default_timezone_set('Europe/Kiev');

/**
 * If session not being started automatically 
 * and if there is no active session identificator, 
 * we start new session. 
 */

if (!get_cfg_var('session.auto_start')) {
    $sessionId = session_id();
    if (empty($sessionId)) {
        session_start();
    }
}

/**
 * Set default permissinons for file creating at 664.
 */
umask(0002);

/**
 * This for PHP 5.2 only, where is no __DIR__ directive. 
 */
if (!defined('__DIR__')) {
    define('__DIR__', dirname(__FILE__));
}

/**
 * Define some constants for system pathes. 
 */
define('DS', DIRECTORY_SEPARATOR);
define('DOCROOT', realpath(__DIR__) . DS);
define('APPPATH', realpath(__DIR__) . DS . 'app' . DS);
define('LIBPATH', realpath(__DIR__) . DS . 'lib' . DS);
define('TPLPATH', realpath(__DIR__) . DS . 'tpl' . DS);
define('LOGPATH', realpath(__DIR__) . DS . 'log' . DS);

/**
 * Register our autoloader. 
 */
require_once(LIBPATH . 'BAutoloader.php');
BAutoloader::addPath(APPPATH, LIBPATH);
BAutoloader::register();

/**
 * Application config file. 
 */
$configFile = DOCROOT . 'config.ini';

/**
 * Create and initialise apllication. 
 */
/* @var $nad NADSystem */
$nad = NADSystem::getInstance($configFile);

NADSystem::getRegistry()->set('DS', DS);
NADSystem::getRegistry()->set('DOCROOT', DOCROOT);
NADSystem::getRegistry()->set('APPPATH', APPPATH);
NADSystem::getRegistry()->set('LIBPATH', LIBPATH);
NADSystem::getRegistry()->set('TPLPATH', TPLPATH);
NADSystem::getRegistry()->set('LOGPATH', LOGPATH);

$nad->init();

//error_reporting($nad->get('error_reporting'));

/**
 * Include some libraries in debug mode. 
 */
if ((int) NADSystem::getRegistry()->get('debug')) {
    require_once(NADSystem::getRegistry()->get('LIBPATH') . 'kint\Kint.class.php');
    //require_once($nad->getRegistry()->get('LIBPATH') . 'PhpConsole.php');
    //PhpConsole::start(true, true, dirname(__FILE__));
}





/**
 * Funny print_r() substitution with show/hide toggler.
 * Supports multiple arguments.
 * 
 * @param One or more arguments for output.
 * @return void 
 */
function pr() {
    $args = func_get_args();
    $id   = time();
    $id  .= mt_rand(0, 1000);
    
    echo <<<EOT
<script>
    if (typeof toggler !== 'function') {
        function toggler(elId) {
            var prevStat = document.getElementById(elId).style.display;
            if (prevStat == 'none') {
                document.getElementById(elId).style.display = 'block';
                document.getElementById(elId + 'Link').innerText = 'hide';
            } else {
                document.getElementById(elId).style.display = 'none';
                document.getElementById(elId + 'Link').innerText = 'show';
            }
        }
    }
</script>
<div id="doWrapper" style="border: 1px dotted #666; background-color: #CCFFCC;padding: 10px; margin: 5px 0">
    <p style="margin: 0; padding: 0; cursor: pointer" onclick="toggler('do{$id}')" title="Click to toggle display state">
        <b>DEBUG OUTPUT</b> <span style="float: right">(click to <span id="do{$id}Link">hide</span>)</span>
    </p>
    <pre id="do{$id}" style="font-size: 12px;">
EOT;

    $c = count($args);
    for ($i = 0; $i < $c; $i++) {
        var_dump($args[$i]);
        if ($i < $c - 1) {
            print_r('<br />');
        }
    }
    echo '</pre></div>';
}