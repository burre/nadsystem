<?php

/**
 * Project:   NAD
 * File:      dbtest.php 
 * Date:      Apr 17, 2012
 * 
 * @package   BTools
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Description of dbtest
 *
 * @uses      parent_class_name
 * @package   package_name 
 * @author    Victor Burak <vb@atompark.com>
 */

set_time_limit(0);
//header('Content-Type: text/html; charset=utf-8'); 

//fwrite(STDOUT, "Daily processing subscriptions from download page...\n");
require_once(dirname(__FILE__) . '/NAD.bootstrap.php');

/* @var $db1 BDatabaseNS */
$db1 = NADSystem::getRegistry()->get('DB1') ;
$db1->query("SELECT * FROM `downloads` WHERE `Email`='dmitriy_almaty@mail.ru' AND `Code`='ROCAXBCFEV'");
$res = $db1->getRowsetAsArray();
//pr($res, $db1->getCharSet());

/* @var $db2 BDatabaseNS */
$db2 = NADSystem::getRegistry()->get('DB2') ;
$db2->query("SELECT * FROM `downloads_templates2` WHERE `id`=6");
$res = $db2->getRowsetAsArray();
//pr($res, $db2->getCharSet());

$d = new NADDownloadModel();
$d->getOneByEmailAndCode('dmitriy_almaty@mail.ru', 'ROCAXBCFEV');
d($d);

$t = new NADTemplateModel();
$t->id = 10;
$t->load();

$d->ID = 209532;
$d->load();
d($t, $d);
$nad->sendFirstMail(209532);

d($db1, $db2);