<?php

/**
 * Project:   NAD - The Notifications After Download System
 * File:      NAD.start.php 
 * Date:      29.03.2012
 * 
 * @package   NAD
 * @version   1.0
 * @copyright Copyright (c) 2012 AtomPark Software Inc.
 * @link      http://atompark.com
 */

/**
 * Starts go-round all of templates for notifications mailing.
 *
 * @package   NAD
 * @author    Victor Burak <vb@atompark.com>
 */

/**
 * Remove time limitation for a script execution. 
 */
set_time_limit(0);
//header('Content-Type: text/html; charset=utf-8'); 

//fwrite(STDOUT, "Daily processing subscriptions from download page...\n");
require_once(dirname(__FILE__) . '/NAD.bootstrap.php');

/* @var $nad NADSystem */
$nad->run();
$nad->beforeExit();
//fwrite(STDOUT, "Done. See log for details.\n");

//d(NADSystem::getRegistry()->get('DB1'));
//d(NADSystem::getRegistry()->get('DB2'));
